package worker

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFib(t *testing.T) {
	var tests = []struct {
		in, out int64
	}{
		{
			in:  0,
			out: 1,
		},
		{
			in:  1,
			out: 1,
		},
		{
			in:  2,
			out: 2,
		},
		{
			in:  7,
			out: 21,
		},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("%v->%v", tt.in, tt.out), func(t *testing.T) {
			assert.Equal(t, tt.out, fib(tt.in))
		})
	}
}
